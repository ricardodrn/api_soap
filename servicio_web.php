<?php
require_once('nusoap/lib/nusoap.php');
include('include/funciones.php');
$miURL = 'http://localhost/ejemplows';
$server = new soap_server();
$server->configureWSDL('miservicioweb', $miURL);
$server->wsdl->schemaTargetNamespace=$miURL;

/*
 *  Ejemplo 1: Enviar_respuesta es una funcion sencilla que recibe un parametro y retorna el mismo
 *  con un string anexado
 */
$server->register('enviar_respuesta', // Nombre de la funcion
				   array('parametro' => 'xsd:string'), // Parametros de entrada
			       array('return' => 'xsd:string'), // Parametros de salida
				   $miURL);
                   

function enviar_respuesta($parametro){
	return new soapval('return', 'xsd:string', 'Hola, esto lo envia el Servidor: '.$parametro);
}

//----------------------------------------------------------------------------------------------
/*
Ejemplo 2: guardo datos que recibo de cualquier dispositivo en la base de datos
*/


$server->register('registrar_datos', // Nombre de la funcion
				   array('parametro' => 'xsd:string','parametro2' => 'xsd:string','parametro3' => 'xsd:string'), // Parametros de entrada
                   array('return' => 'xsd:string'), // Parametros de salida
               	   $miURL);

function registrar_datos($parametro,$parametro2,$parametro3){
    
    //recibo el dato enviado por el celular, ahora pongo un mensaje en la variable_accion
    
    $indicador_registro="No";
    
    include ("include/funciones.inc"); // llama el archivo funciones.inc donde le hace la conexion con la BD
    $link =conectar(); // Se llama la funcion conectar(); que establece la conexi?n
    mysql_select_db("ventas", $link);//Fuci?nque seleciona la base de datos
   
          
            $cad="insert into datos values ('0','$parametro','$parametro2','$parametro3')";
           
           
           if($result= mysql_query ($cad, $link))  //ejecut la consulta a la base de datos
           {
             $indicador_registro="Si";
           }
           else{
            print mysql_error();//Imprime un mensaje error en el caso de algun problem
            }           
            
	return new soapval('return', 'xsd:string',$indicador_registro);
}

//***********************************************************************************
               
/*
Ejemplo 3: Busco los datos a traves de la cedula que recibo como parametro
*/
//########################################################

$server->register('buscar_datos', // Nombre de la funcion
				   array('cedula' => 'xsd:string'), // Parametros de entrada
                   array('return' => 'xsd:string'), // Parametros de salida
               	   $miURL);               
            
function buscar_datos($cedula){
    
    //recibo el dato enviado por el celular, ahora pongo un mensaje en la variable_accion
    
    $encontro="No";
    
    include ("include/funciones.inc"); // llama el archivo funciones.inc donde le hace la conexion con la BD
    $link =conectar(); // Se llama la funcion conectar(); que establece la conexi?n
    mysql_select_db("ventas", $link);//Fuci?nque seleciona la base de datos
   
     $recibe = "select * from datos where cedula='$cedula'";//string que almacena l aconsulta a ejecutar
     $result= mysql_query ($recibe, $link);//ejecut la consulta a la base de datos
         
        while ($f=mysql_fetch_row($result)){ // Convertimos el resultado en un vector
             $encontro="Si";
         }          
          
       
	return new soapval('return', 'xsd:string',$encontro);
}   


//############### Si los datos fueron encontrados procedo a mostrarlos independientemente ###############
  
//                             Muestro La Cedula                   //  
               
$server->register('mostrar_datos_cedula', // Nombre de la funcion
				   array('cedula' => 'xsd:string'), // Parametros de entrada
                   array('return' => 'xsd:string'), // Parametros de salida
               	   $miURL);               
            
function mostrar_datos_cedula($cedula){
    
    //recibo el dato enviado por el celular, ahora pongo un mensaje en la variable_accion
        
    include ("include/funciones.inc"); // llama el archivo funciones.inc donde le hace la conexion con la BD
    $link =conectar(); // Se llama la funcion conectar(); que establece la conexi?n
    mysql_select_db("ventas", $link);//Fuci?nque seleciona la base de datos
   
     $recibe = "select * from datos where cedula='$cedula'";//string que almacena l aconsulta a ejecutar
     $result= mysql_query ($recibe, $link);//ejecut la consulta a la base de datos
         
        while ($f=mysql_fetch_row($result)){ // Convertimos el resultado en un vector
            $valor_retorno= $f[1];       
        }          
          
       
	return new soapval('return', 'xsd:string',$valor_retorno);
}               
               
               
//############### Si los datos fueron encontrados procedo a mostrarlos independientemente ###############

//                             Muestro el Nombre                   //  

$server->register('mostrar_datos_nombre', // Nombre de la funcion
				   array('cedula' => 'xsd:string'), // Parametros de entrada
                   array('return' => 'xsd:string'), // Parametros de salida
               	   $miURL);               
            
function mostrar_datos_nombre($cedula){
    
    //recibo el dato enviado por el celular, ahora pongo un mensaje en la variable_accion

    include ("include/funciones.inc"); // llama el archivo funciones.inc donde le hace la conexion con la BD
    $link =conectar(); // Se llama la funcion conectar(); que establece la conexi?n
    mysql_select_db("ventas", $link);//Fuci?nque seleciona la base de datos
   
     $recibe = "select * from datos where cedula='$cedula'";//string que almacena l aconsulta a ejecutar
     $result= mysql_query ($recibe, $link);//ejecut la consulta a la base de datos
         
        while ($f=mysql_fetch_row($result)){ // Convertimos el resultado en un vector
            $valor_retorno= $f[2];
                  
        }          
          
       
	return new soapval('return', 'xsd:string',$valor_retorno);
}

//############### Si los datos fueron encontrados procedo a mostrarlos independientemente ###############

//                             Muestro Los Apellidos                  //  

$server->register('mostrar_datos_apellido', // Nombre de la funcion
				   array('cedula' => 'xsd:string'), // Parametros de entrada
                   array('return' => 'xsd:string'), // Parametros de salida
               	   $miURL);               
            
function mostrar_datos_apellido($cedula){
    
    //recibo el dato enviado por el celular, ahora pongo un mensaje en la variable_accion
    
    include ("include/funciones.inc"); // llama el archivo funciones.inc donde le hace la conexion con la BD
    $link =conectar(); // Se llama la funcion conectar(); que establece la conexi?n
    mysql_select_db("ventas", $link);//Fuci?nque seleciona la base de datos
   
     $recibe = "select * from datos where cedula='$cedula'";//string que almacena l aconsulta a ejecutar
     $result= mysql_query ($recibe, $link);//ejecut la consulta a la base de datos
         
        while ($f=mysql_fetch_row($result)){ // Convertimos el resultado en un vector
            $valor_retorno= $f[3];      
        }          
           
	return new soapval('return', 'xsd:string',$valor_retorno);
}          
//Login 

$server->register('login', // Nombre de la funcion
				   array('email' => 'xsd:string', 'password' => 'xsd:string' ), // Parametros de entrada
                   array('return' => 'xsd:string'), // Parametros de salida
               	   $miURL);               
            
function login($email, $password){
        
    $encontro="No";
    
    include ("include/funciones.inc"); // llama el archivo funciones.inc donde le hace la conexion con la BD
    $link =conectar(); // Se llama la funcion conectar(); que establece la conexi?n
    mysql_select_db("ventas", $link);//Fuci?nque seleciona la base de datos
   
     $recibe = "SELECT * FROM users WHERE email = '$email';";//string que almacena l aconsulta a ejecutar
     $result= mysql_query ($recibe, $link);//ejecut la consulta a la base de datos
	 $user =  mysql_fetch_array($result);

		if (sizeof($user) > 0) {
			// verifying user password
			$salt = $user['salt'];
			$encrypted_password = $user['encrypted_password'];
			$hash = checkhashSSHA($salt, $password);
			// check for password equality
			if ($encrypted_password == $hash) {
				// user authentication details are correct
				$encontro="Si";
			}
		}  
	return new soapval('return', 'xsd:string',$encontro);
}                                    
///recupera arreglo

           
//......................................................................................               
$HTTP_RAW_POST_DATA = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : '';
$server->service($HTTP_RAW_POST_DATA);
// $server->service('php://input');
?>