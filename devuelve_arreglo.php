<?php
	require_once('nusoap/lib/nusoap.php');
	include('include/funciones.php');
	$miURL = 'http://localhost/ejemplows';
	$server = new soap_server();
	// $server->configureWSDL('serviciodevuelve', $miURL);
	// $server->wsdl->schemaTargetNamespace=$miURL;
	   
   $SERVICE_NAMESPACE = "urn:Testing_Service"; // create a namespace to run under.
   // this has many input parameters but we only need two: the service name and the namespace
   $server->configureWSDL('Testing_Service', $SERVICE_NAMESPACE);
	
	
	// complex types are like 'struct' in C#.... it's a way to bind an object with different properties and variables together.    
$server->wsdl->addComplexType(
    'MyTableData', // the type's name
    'complexType', // yes.. indeed it is a complex type.
    'struct', // php it's a structure. (only other option is array) 
    'all', // compositor.. 
    '',// no restriction
    array(
        'lId' => array('name'=>'lId','type'=>'xsd:string'),
        'sValue' => array('name'=>'sValue','type'=>'xsd:string'),
        'sText' => array('name'=>'sText','type'=>'xsd:string')
    )// the elements of the structure.
);
    
// Here we need to make another complex type of our last complex type.. but now an array!
$server->wsdl->addComplexType(
    'MyTableArray',//glorious name
    'complexType',// not a simpletype for sure!
    'array',// oh we are an array now!
    '',// bah. blank
    'SOAP-ENC:Array',
	array(),// our element is an array.
     array(array('ref'=>'SOAP-ENC:arrayType','wsdl:arrayType'=>'tns:MyTableData[]')),//the attributes of our array.
    'tns:MyTableData'// what type of array is this?  Oh it's an array of mytable data
);
    
  

       // Register a method name with the service and make it publicly accessable.
   $server->register('Get_Test_Data',// method name
        array(),// input parameter - nothing!
        array('return' => 'tns:MyTableArray'),// output - object of type MyTableArray.
        $SERVICE_NAMESPACE,// namespace
       false,// soapaction
        'rpc',// style.. remote procedure call
        false,// use of the call
        ' Get all the data from test_table.'// documentation for people who hook into your service.
    );
    
        // here is the method you registered.. it takes in nothing and returns an array of MyTableArray
    function Get_Test_Data() 
    {
				// $oReturn[] = array();
				// woah who has a super unsecure connection to his database?  This guy!
				include ("include/funciones.inc"); // llama el archivo funciones.inc donde le hace la conexion con la BD
				$link =conectar(); // Se llama la funcion conectar(); que establece la conexi?n
				mysql_select_db("ventas", $link);//Fuci?nque seleciona la base de datos     
				
				// a simple select statement.
				$sql = "SELECT id
							,name
							,salt
							FROM users; ";
    
				//fill the output into something we can use.
    			$result = mysql_query($sql) or die('Query failed: ' . mysql_error());
    			// it's always nice to know how many rows we have.
    			$num_rows = mysql_num_rows($result);
    		
    			//make a generic array of the size of our result set.
    			$oReturn[$num_rows] = array();
    			// create a counter so we can traverse each item in our array.
    			$lCounter = 0;
    			
				//while i can still get data back    			
				while ($line = mysql_fetch_array($result, MYSQL_ASSOC)) 
				{
					// for the given index assign it an array of data.. specifically telling it what column gets what chunk of data.
					$oReturn[$lCounter] = array('lId'=>$line['id'], 'sValue'=>$line['name'] , 'sText'=>$line['salt']);
					// increment the counter for the next run.
					$lCounter = $lCounter+1;
				}
	// return it all.   			
      return $oReturn;
    }

	
	
	
	$HTTP_RAW_POST_DATA = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : '';
	$server->service($HTTP_RAW_POST_DATA);

?>